#include "os_memory.h"
#include "os_memory_strategies.h"
#include "util.h"
#include "os_core.h"

//Nibble Manipulation Operations (getters and setters)
void setLowNibble(Heap const *heap, MemAddr addr, MemValue value) {
    *(uint8_t * )(addr) &= 0b11110000;
    //ensure that only the lower part of the value is considered, so that regardless of the value of value only the lower nibble is changed
    value = value & 0b00001111;

    *(uint8_t * )(addr) |= value;
}

void setHighNibble(Heap const *heap, MemAddr addr, MemValue value) {
    //ensure that only the higher part of the value is considered, so that regardless of the value of value only the lower nibble is changed
    //value = value | 0b00001111;
    *(uint8_t * )(addr) &= 0b00001111;
    value = value<<4;

    *(uint8_t * )(addr) |= value;
}

void setMapEntry(Heap const *heap, MemAddr addr, MemValue value) {
    uint16_t addressRange = addr - heap->useStartingAddress;
    bool isOnHighNibble = addressRange % 2 == 0;
    addressRange = addressRange / 2; // cuz each address in map is mapped with 2 in use space
    MemAddr mapAddress = heap->mapStartingAddress + addressRange; // located mapping byte

    if (isOnHighNibble) setHighNibble(heap, mapAddress, value);
    else setLowNibble(heap, mapAddress, value);
}

MemValue getLowNibble(Heap const *heap, MemAddr addr) {
    return *(uint8_t * )(addr) & 0b00001111;
}

MemValue getHighNibble(Heap const *heap, MemAddr addr) {
    return (*(uint8_t * )(addr) & 0b11110000) >> 4;
}

/*!
 * This function is used to get a heap map entry on a specific heap
 * @param heap The heap from whose map the entry is supposed to be fetched
 * @param addr The address in USE SPACE for which the corresponding map entry shall be fetched
 * @return The value that can be found on the heap map entry that corresponds to the given use space address
 */
MemValue os_getMapEntry(Heap const *heap, MemAddr addr) {
    uint16_t addressRange = addr - heap->useStartingAddress;
    bool isOnHighNibble = addressRange % 2 == 0;
    addressRange = addressRange / 2 ; // cuz each address in map is mapped with 2 in use space
    MemAddr mapAddress = heap->mapStartingAddress + addressRange; // located mapping byte

    if (isOnHighNibble) return getHighNibble(heap, mapAddress);
    return getLowNibble(heap, mapAddress);
}

size_t os_getMapSize(Heap const *heap) {
    return heap->mapSize;
}

size_t os_getUseSize(Heap const *heap) {
    return heap->useSize;
}

MemAddr os_getMapStart(Heap const *heap) {
    return heap->mapStartingAddress;
}

MemAddr os_getUseStart(Heap const *heap) {
    return heap->useStartingAddress;
}

AllocStrategy os_getAllocationStrategy(Heap const *heap) {
    return heap->currentAllocationStrategy;
}

void os_setAllocationStrategy(Heap *heap, AllocStrategy allocStrat) {
    heap->currentAllocationStrategy = allocStrat;
}

//Chunk manipulation operations
uint16_t os_getChunkSize(Heap const *heap, MemAddr addr) {
    MemAddr address = os_getFirstByteOfChunk(heap, addr);
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    uint16_t counter = 0;
    if(os_getMapEntry(heap,addr) == 0) {
        while (os_getMapEntry(heap, address) == 0 && address <= useEndAddress) {
            counter++;
            address++;
        }
        return counter;
    }
    else{
        // First Byte in Map-chunk is Nibble, in which pid is saved
		counter++;
		address++;
        while (os_getMapEntry(heap, address) == 0xF && address <= useEndAddress) {
            counter++;
            address++;
        }
        return counter;
    }
}

MemAddr os_getFirstByteOfChunk(Heap const *heap, MemAddr addr) {
    MemAddr address = addr;
    if(os_getMapEntry(heap,addr) == 0){
        while (os_getMapEntry(heap, address) == 0 && address >= heap->useStartingAddress) address--;
        address++;
        return address;
    }

    else{
        while (os_getMapEntry(heap, address) == 0xF && address >= heap->useStartingAddress) address--;
        return address;
    }
}

ProcessID getOwnerOfChunk(Heap const *heap, MemAddr addr) {
    MemValue valueOfFirstByte = os_getMapEntry(heap, os_getFirstByteOfChunk(heap, addr));
    switch (valueOfFirstByte) {
        case 0:
            return 0;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 2;
            break;
        case 3:
            return 3;
            break;
        case 4:
            return 4;
            break;
        case 5:
            return 5;
            break;
        case 6:
            return 6;
            break;
        case 7:
            return 7;
            break;
        default:
            os_errorPStr("First Byte of chunk wrongly identified");
    }
    return 0;
}

void os_freeOwnerRestricted(Heap *heap, MemAddr addr, ProcessID owner) {
    if (owner != getOwnerOfChunk(heap, addr)) return;
    else {
        MemAddr currAddr = os_getFirstByteOfChunk(heap, addr);
        os_enterCriticalSection();
        setMapEntry(heap, currAddr, 0);
        currAddr++;
        while (os_getMapEntry(heap, currAddr) == 0xF && currAddr<heap->useStartingAddress+heap->useSize) {
            setMapEntry(heap, currAddr, 0);
            currAddr++;
        }
        os_leaveCriticalSection();
    }
}

void os_free(Heap *heap, MemAddr addr) {
    os_freeOwnerRestricted(heap,addr,os_getCurrentProc());
}

void os_freeProcessMemory(Heap *heap, ProcessID pid) {
    for(size_t i=0;i<heap->useSize;i++){
        if(getOwnerOfChunk(heap,heap->useStartingAddress+i)==pid) os_freeOwnerRestricted(heap,heap->useStartingAddress+i,pid);
    }
}

MemAddr os_malloc(Heap *heap, uint16_t size) {
	os_enterCriticalSection();
    MemAddr startAddress = 0;
    switch (heap->currentAllocationStrategy) {
        case OS_MEM_FIRST:
            startAddress = os_Memory_FirstFit(heap, size);
            break;
/*
        case OS_MEM_NEXT:
            startAddress = os_Memory_NextFit(heap, size);
            break;

        case OS_MEM_BEST:
            startAddress = os_Memory_BestFit(heap, size);
            break;

        case OS_MEM_WORST:
            startAddress = os_Memory_WorstFit(heap, size);
            break;
*/
        default:
            os_errorPStr("Problems with identifying currAllocStrat");
    }
    if(startAddress==0) {
        os_leaveCriticalSection();
        return 0;
    }
    setMapEntry(heap,startAddress,os_getCurrentProc());
    for(uint16_t i=1;i<size;i++){
        setMapEntry(heap,startAddress+i,0xF);
    }
    os_leaveCriticalSection();
    return startAddress;
}