#include "os_memheap_drivers.h"
#include "defines.h"
#include "os_memory_strategies.h"
#include <avr/pgmspace.h>

static char const PROGMEM
intStr [] = "internal";

#define __heap_start AVR_SRAM_START + HEAPOFFSET

Heap intHeap__ = {
        .driver = intSRAM,
        .mapStartingAddress = __heap_start,
        .mapSize= (AVR_SRAM_LAST - __heap_start) / (3),
        .useStartingAddress = __heap_start + ((AVR_SRAM_LAST - __heap_start) / (3)),
        .useSize=2*(AVR_SRAM_LAST - __heap_start) / (3),
        .currentAllocationStrategy=OS_MEM_FIRST,
        .name = intStr
};

void os_initHeaps() {
    for (size_t i = 0; i < os_getHeapListLength(); i++) {
        Heap* currHeap = os_lookupHeap(i);
        for (size_t j = 0; j < (currHeap->mapSize); j++) {
            currHeap->driver->write(currHeap->mapStartingAddress + j, 0);
        }
    }
}

size_t os_getHeapListLength(void) {
    return sizeof(intHeap) / sizeof(Heap*);
}

Heap *os_lookupHeap(uint8_t index) {
    return intHeap + index * sizeof(Heap);
}
