#include "os_memory.h"
#include "os_memory_strategies.h"
#include "os_memheap_drivers.h"

MemAddr os_Memory_FirstFit(Heap *heap, size_t size) {
    MemAddr currAdrr = heap->useStartingAddress;
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    size_t  currChunkSize;

    while (currAdrr <= useEndAddress) {
        currChunkSize = os_getChunkSize(heap, currAdrr);
        if (os_getMapEntry(heap, currAdrr) == 0 && currChunkSize >= size) {
            return currAdrr;
        }
        currAdrr += currChunkSize;
    }
    return 0;
}

MemAddr os_Memory_NextFit (Heap *heap, size_t size) {
    MemAddr currAdrr = os_Memory_FirstFit(heap, size);
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    size_t  currChunkSize;

    while (currAdrr <= useEndAddress) {
        currChunkSize = os_getChunkSize(heap, currAdrr);
        if (os_getMapEntry(heap, currAdrr) == 0 && currChunkSize >= size) {
            return currAdrr;
        }
        currAdrr += currChunkSize;
    }
    return 0;
}

MemAddr os_Memory_BestFit (Heap *heap, size_t size) {
    MemAddr currAdrr = heap->useStartingAddress;
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    MemAddr bestFitAddr = 0;
    size_t currChunkSize;
    size_t minDiffChunkSize = SIZE_MAX;

    while (currAdrr <= useEndAddress) {
        currChunkSize = os_getChunkSize(heap, currAdrr);
        if (os_getMapEntry(heap, currAdrr) == 0 && currChunkSize >= size) {
            if (currChunkSize - size < minDiffChunkSize) {
                minDiffChunkSize = currChunkSize - size;
                bestFitAddr = currAdrr;
            }
        }
        currAdrr += currChunkSize;
    }
    return bestFitAddr;
}
/*
MemAddr os_Memory_WorstFit (Heap *heap, size_t size) {
    MemAddr currAdrr = heap->useStartingAddress;
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    MemAddr worstFitAddr = 0;
    size_t currChunkSize;
    size_t maxDiffChunkSize = 0;

    while (currAdrr <= useEndAddress) {
        currChunkSize = os_getChunkSize(heap, currAdrr);
        if (os_getMapEntry(heap, currAdrr) == 0 && currChunkSize >= size) {
            if (currChunkSize - size > maxDiffChunkSize) {
                maxDiffChunkSize = currChunkSize - size;
                worstFitAddr = currAdrr;
            }
        }
        currAdrr += currChunkSize;
    }
    return worstFitAddr;
}*/