#include "os_mem_drivers.h"
#include "atmega644constants.h"

//Adresse des Treibers
#define intSRAM (&intSRAM__)


//Treiberfunktionen -- pseudo-function -- nothing to do with
static void initSRAM_internal(void) {}

/*!
 * Read value from address in Medium
 * @param addr address in Medium
 * @return value in given address in Medium
 */
static MemValue readSRAM_internal(MemAddr addr) {
    return *(uint8_t *) addr;
}

/*!
 * Routine that saves value in Medium
 * @param addr The address on Medium the value shall be written to
 * @param value The value to be written
 */
static void writeSRAM_internal(MemAddr addr, MemValue value) {
    *((uint8_t *) addr) = value;
}

// Treiberinstanz
MemDriver intSRAM__ = {
        .init = initSRAM_internal,
        .read = readSRAM_internal,
        .write = writeSRAM_internal,
        .start = AVR_SRAM_START,
        .size = AVR_MEMORY_SRAM
};